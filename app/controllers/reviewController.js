const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const mongoose = require("mongoose");


//Create
const createReviewOfCourse = (request, response) => {

    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    let requestBody = request.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is invalid"
        })
    }

    if (!(Number.isInteger(requestBody.stars) && requestBody.stars > 0 && requestBody.stars <= 5)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "stars is invalid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let newReviewInput = {
        _id: mongoose.Types.ObjectId(),
        stars: requestBody.stars,
        note: requestBody.note
    }

    reviewModel.create(newReviewInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            courseModel.findByIdAndUpdate(courseId, {
                    $push: { reviews: data._id }
                },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Review Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllReviewOfCourse = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId)
        .populate("reviews")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.reviews
                })
            }
        })
}

const getReviewById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let reviewId = request.params.reviewId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.findById(reviewId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get review success",
                data: data
            })
        }
    })
}

const updateReviewById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let reviewId = request.params.reviewId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let reviewUpdate = {
        stars: bodyRequest.rate,
        node: bodyRequest.note
    }

    reviewModel.findByIdAndUpdate(reviewId, reviewUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update review success",
                data: data
            })
        }
    })
}

const deleteReviewById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    let reviewId = request.params.reviewId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 review khỏi collection cần xóa thêm reviewID trong course đang chứa nó
            courseModel.findByIdAndUpdate(courseId, {
                    $pull: { reviews: reviewId }
                },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete review success"
                        })
                    }
                })
        }
    })
}

module.exports = {
    createReviewOfCourse: createReviewOfCourse,
    getAllReviewOfCourse: getAllReviewOfCourse,
    getReviewById: getReviewById,
    updateReviewById: updateReviewById,
    deleteReviewById: deleteReviewById
}